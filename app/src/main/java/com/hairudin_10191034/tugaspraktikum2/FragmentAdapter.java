package com.hairudin_10191034.tugaspraktikum2;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class FragmentAdapter extends FragmentPagerAdapter {
    private Context context;
    private int tabscount;
    public FragmentAdapter (Context context, int tabscount, FragmentManager fragmentmanager){
        super(fragmentmanager);

        this.context=context;
        this.tabscount=tabscount;

    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch(position){
            case 0:
                HomeFragment homeFragment=new HomeFragment();
                return homeFragment;
            case 1:
                StatusFragment statusFragment=new StatusFragment();
                return statusFragment;
            default:
                return null;

        }
    }

    @Override
    public int getCount() {
        return tabscount;
    }
}   